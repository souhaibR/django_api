FROM python:3.8

LABEL version="1.0" maintainer="souhaib"

ARG APT_FLAGS="-q -y"

#environment variables  

ENV PYTHONUNBUFFERED 1

# source de l application 
ENV Source=/home/django_api_ml

# clone le projet 
RUN apt-get update -y
RUN apt-get install ffmpeg libsm6 libxext6  -y 
RUN mkdir -p $Source
RUN git clone https://gitlab.com/souhaibR/django_api_ml.git $Source

#dossier de travail 
WORKDIR $Source
 
#update pip
RUN python -m pip install --upgrade pip

#execute les dependences 
RUN pip install -r requirements.txt

#Port 
EXPOSE 8000

#start server 
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]

