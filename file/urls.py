from django.urls import path, include
from rest_framework import routers
from .views import UploadViewSet

router = routers.DefaultRouter()
router.register(r'file', UploadViewSet, basename="file")

# Wire up our API using automatic URL routing.
urlpatterns = [
    path('', include(router.urls)),
    path('stat', UploadViewSet.stat, name="stat"),
    path('empty_stat', UploadViewSet.empty_stat, name="empty_stat"),
    
    
    
]